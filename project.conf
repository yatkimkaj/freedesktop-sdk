name: freedesktop-sdk

format-version: 12

aliases:
  flathub: https://flathub.org/
  freedesktop: https://gitlab.freedesktop.org/
  freedesktop-old: https://anongit.freedesktop.org/git/
  ftp_gnu_org: https://ftp.gnu.org/gnu/
  savannah: https://git.savannah.gnu.org/git/
  github: https://github.com/
  gitlab: https://gitlab.com/
  gnome: https://gitlab.gnome.org/GNOME/
  gnupg: https://dev.gnupg.org/source/
  perl5: https://perl5.git.perl.org/perl.git
  ffmpeg: https://git.ffmpeg.org/
  sourceware: https://sourceware.org/git/
  sourceware_pub: https://sourceware.org/pub/
  nettle: https://git.lysator.liu.se/nettle/
  sourceforge: https://sourceforge.net/
  sourceforge_download: https://downloads.sourceforge.net/
  freedesktop-sdk: https://cache.sdk.freedesktop.org/

mirrors:
  - name: kernel_org
    aliases:
      ftp_gnu_org:
      - https://mirrors.kernel.org/gnu/
  - name: github_mirrors
    aliases:
      perl5:
      - https://github.com/Perl/perl5.git
      ffmpeg:
      - https://github.com/FFmpeg/
  - name: freedesktop_sdk_mirrors
    aliases:
      sourceware:
      - https://gitlab.com/freedesktop-sdk/mirrors/
  - name: nettle_mirrors
    aliases:
      nettle:
      - https://gitlab.com/gnutls/

element-path: elements

fail-on-overlap: true

variables:
  sysroot: /cross-installation
  tools: /cross

  builddir: bst_build_dir
  conf-deterministic: |
    --enable-deterministic-archives
  conf-link-args: |
    --enable-shared \
    --disable-static
  conf-host: |
    --host=%{host-triplet}
  conf-build: |
    --build=%{build-triplet}
  conf-libtool-force-dlsearch: |
    lt_cv_sys_lib_dlsearch_path_spec="/usr/lib/%{gcc_triplet}"
  host-triplet: "%{triplet}"
  build-triplet: "%{triplet}"
  guessed-triplet: "$(sh /usr/share/gnu-config/config.guess)"
  sbindir: "%{bindir}"
  sysconfdir: "/etc"
  localstatedir: "/var"
  branch: "19.08"
  snap-branch: "19-08"
  lib: "lib/%{gcc_triplet}"
  indep-libdir: "%{prefix}/lib"
  debugdir: "%{indep-libdir}/debug"
  sourcedir: "%{debugdir}/source"
  gcc_triplet: "%{gcc_arch}-linux-%{abi}"
  triplet: "%{arch}-unknown-linux-%{abi}"
  gcc_arch: "%{arch}"
  abi: "gnu"
  snap_arch: "%{arch}"

  debug_flags: "-g"
  common_flags: "-O2 %{debug_flags} -pipe"
  build_common_flags: "%{common_flags}"
  build_flags_x86_64: "%{build_common_flags}"
  build_flags_i686: "%{build_common_flags}"
  build_flags_aarch64: "%{build_common_flags}"
  build_flags_arm: "%{build_common_flags}"
  target_common_flags: "%{common_flags} -Wp,-D_FORTIFY_SOURCE=2 -Wp,-D_GLIBCXX_ASSERTIONS -fexceptions -fstack-protector-strong -grecord-gcc-switches"
  target_flags_x86_64: "%{target_common_flags} -fasynchronous-unwind-tables -fstack-clash-protection -fcf-protection"
  # -mstackrealign is to  a potential but unlikely bug introduced
  # by the change of the Linux i386 ABI a long time ago. Stack used to be
  # 4-bytes aligned, now it is 16-bytes aligned for SSE. Not available
  # by default. So it is passed in flags.
  # See https://gcc.gnu.org/bugzilla/show_bug.cgi?id=40838
  target_flags_i686: "-mstackrealign %{target_common_flags} -fasynchronous-unwind-tables -fstack-clash-protection -fcf-protection"
  target_flags_aarch64: "%{target_common_flags} -fasynchronous-unwind-tables -fstack-clash-protection"
  target_flags_arm: "%{target_common_flags}"
  ldflags_defaults: "-Wl,-z,relro,-z,now -Wl,--as-needed"

  (?):
  - target_arch == "i686":
      gcc_arch: "i386" # See https://wiki.debian.org/Multiarch/Tuples#Why_not_use_GNU_triplets.3F
      snap_arch: "i386"
  - target_arch == "arm":
      abi: "gnueabihf"
      snap_arch: "armhf"
  - target_arch == "x86_64":
      snap_arch: "amd64"
  - target_arch == "aarch64":
      snap_arch: "arm64"

  ca_path: "%{sysconfdir}/ssl/certs/ca-certificates.crt"


  # Arguments for tooling used when stripping debug symbols
  objcopy-link-args: --add-gnu-debuglink
  objcopy-extract-args: |

    --only-keep-debug --compress-debug-sections

  strip-args: |

    --remove-section=.comment --remove-section=.note --strip-unneeded --remove-section=.gnu_debugaltlink

  optimize-debug: "true"

  strip-binaries: |
    touch source-files
    to_strip=()
    first_found_target=""
    multiple_targets=false
    find "%{install-root}" -type f \
      '(' -perm -111 -o -name '*.so*' \
          -o -name '*.cmxs' -o -name '*.node' ')' \
          -print0 >binaries
    while read -r -d $'\0' file; do
      read -n4 hdr <"${file}" || continue # check for elf header
      if [ "$hdr" != "$(printf \\x7fELF)" ]; then
        continue
      fi
      if [ -x "$(command -v identify-elf)" ]; then
         target_triplet="$(identify-elf "${file}")"
      else
         target_triplet="%{host-triplet}"
      fi
      if [ -z "${first_found_target}" ]; then
        first_found_target="${target_triplet}"
      elif [ "${target_triplet}" != "${first_found_target}" ]; then
        multiple_targets=true
      fi
      toolchain=""
      for p in "%{tools}" "/usr" "%{prefix}"; do
        if [ -x "${p}/${target_triplet}/bin/objdump" ]; then
          toolchain="${p}/${target_triplet}/bin/"
          break
        fi
      done
      if "${toolchain}objdump" -j .gnu_debuglink -s "${file}" &>/dev/null; then
        continue
      fi
      case "${file}" in
        "%{install-root}%{debugdir}/"*)
          continue
          ;;
        *)
          ;;
      esac
      if [ "$(stat -c "%h" "${file}")" != "1" ]; then
        echo "Stripping files with multiple links can be problematic for reproducibility." 1>&2
        echo "File ${file} has multiple links" 1>&2
        false
      fi
      if [ -x "$(command -v debugedit)" ]; then
        debugedit -i --list-file=source-files.part --base-dir="%{build-root}" --dest-dir="%{sourcedir}/%{element-name}" "${file}"
        cat source-files.part >>source-files
      fi
      to_strip+=("${file}:${toolchain}")
    done <binaries
    rm binaries
    if [ "%{optimize-debug}" = "true" ] && \
       [ "${#to_strip[@]}" -ge 1 ] && \
       [ -x "$(command -v eu-elfcompress)" ] && \
       [ -x "$(command -v dwz)" ]; then
      for file in "${to_strip[@]}"; do
        eu-elfcompress --type=none "${file/:*/}"
      done
      dwzdir="%{debugdir}/dwz/%{element-name}"
      if [ "${#to_strip[@]}" -gt 1 ] && [ "${multiple_targets}" = false ]; then
        mkdir -p "%{install-root}${dwzdir}"
        dwz -m "%{install-root}${dwzdir}/debug" -M "${dwzdir}/debug" "${to_strip[@]/:*/}"
      elif [ "${#to_strip[@]}" -eq 1 ]; then
        dwz "${to_strip[@]/:*/}"
      fi
    fi
    for file in "${to_strip[@]}"; do
      toolchain="${file/*:/}"
      file="${file/:*/}"
      realpath="$(realpath -s --relative-to="%{install-root}" "${file}")"
      debugfile="%{install-root}%{debugdir}/${realpath}.debug"
      mkdir -p "$(dirname "$debugfile")"
      "${toolchain}objcopy" %{objcopy-extract-args} "${file}" "$debugfile"
      chmod 644 "$debugfile"
      mode="$(stat -c 0%a "${file}")"
      [ -w "${file}" ] || chmod +w "${file}"
      "${toolchain}strip" %{strip-args} "${file}"
      "${toolchain}objcopy" %{objcopy-link-args} "$debugfile" "${file}"
      chmod "${mode}" "${file}"
      eu-elfcompress "${file}"
    done
    sort -zu  <source-files | while read -r -d $'\0' source; do
      dst="%{install-root}%{sourcedir}/%{element-name}/${source}"
      src="%{build-root}/${source}"
      if [ -d "${src}" ]; then
        install -m0755 -d "${dst}"
        continue
      fi
      [ -f "${src}" ] || continue
      install -m0644 -D "${src}" "${dst}"
    done

environment:
  (?):
    - target_arch == "x86_64":
        CFLAGS:  "%{target_flags_x86_64}"
        CXXFLAGS: "%{target_flags_x86_64}"
        LDFLAGS:  "%{ldflags_defaults}"
    - target_arch == "i686":
        CFLAGS: "%{target_flags_i686}"
        CXXFLAGS: "%{target_flags_i686}"
        LDFLAGS:  "%{ldflags_defaults}"
    - target_arch == "arm":
        CFLAGS:  "%{target_flags_arm}"
        CXXFLAGS: "%{target_flags_arm}"
        LDFLAGS:  "%{ldflags_defaults}"
    - target_arch == "aarch64":
        CFLAGS:  "%{target_flags_aarch64}"
        CXXFLAGS: "%{target_flags_aarch64}"
        LDFLAGS:  "%{ldflags_defaults}"
  LC_ALL: en_US.UTF-8
  PYTHON: "%{bindir}/python3"

  # Python documentation "Hash randomization is intended to provide
  # protection against a denial-of-service caused by carefully-chosen
  # inputs that exploit the worst case performance of a dict
  # construction, O(n^2) complexity. See
  # http://www.ocert.org/advisories/ocert-2011-003.html for details."
  # The attack case isn't relevant for code compilation and this breaks
  # reproducible pycs. Setting seed to 0 is the canonical way to disable
  # hash randomization
  PYTHONHASHSEED: 0

split-rules:
  devel:
    - "%{includedir}"
    - "%{includedir}/**"
    - "%{libdir}/pkgconfig"
    - "%{libdir}/pkgconfig/**"
    - "%{datadir}/pkgconfig"
    - "%{datadir}/pkgconfig/**"
    - "%{datadir}/aclocal"
    - "%{datadir}/aclocal/**"
    - "%{prefix}/lib/cmake"
    - "%{prefix}/lib/cmake/**"
    - "%{libdir}/cmake"
    - "%{libdir}/cmake/**"
    - "%{prefix}/lib/*.a"
    - "%{libdir}/*.a"

  debug:
    - "%{debugdir}/**"

plugins:
  - origin: local
    path: plugins/sources
    sources:
      crate: 0

  - origin: local
    path: plugins/elements
    elements:
      check_forbidden: 0
      snap_image: 0

  - origin: pip
    package-name: buildstream-external
    elements:
      collect_integration: 0
      collect_manifest: 0
      flatpak_image: 0
      flatpak_repo: 0
      tar_element: 0
      x86image: 0
    sources:
      git_tag: 1

options:
  bootstrap_build_arch:
    type: arch
    description: Architecture
    variable: bootstrap_build_arch
    values:
      - arm
      - aarch64
      - i686
      - x86_64

  target_arch:
    type: arch
    description: Architecture
    variable: arch
    values:
      - arm
      - aarch64
      - i686
      - x86_64

  snap_grade:
    type: enum
    description: Snap grade level (devel or stable)
    variable: snap_grade
    default: devel
    values:
      - devel
      - stable

artifacts:
  url: https://freedesktop-sdk-cache.codethink.co.uk:11001

elements:
  cmake:
    variables:
      generator: Ninja
      cmake-global: |
        -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON
  autotools:
    variables:
      remove_libtool_modules: "true"
      remove_libtool_libraries: "true"
      delete_libtool_files: |
          find "%{install-root}" -name "*.la" -print0 | while read -d '' -r file; do
            if grep '^shouldnotlink=yes$' "${file}" &>/dev/null; then
              if %{remove_libtool_modules}; then
                echo "Removing ${file}."
                rm "${file}"
              else
                echo "Not removing ${file}."
              fi
            else
              if %{remove_libtool_libraries}; then
                echo "Removing ${file}."
                rm "${file}"
              else
                echo "Not removing ${file}."
              fi
            fi
          done
      conf-global: |
        %{conf-deterministic} \
        %{conf-link-args} \
        %{conf-build} \
        %{conf-host} \
        %{conf-libtool-force-dlsearch}
      conf-cmd: configure
    config:
      configure-commands:
        - |
          %{autogen}
          if [ -n "%{builddir}" ]; then
            mkdir %{builddir}
            cd %{builddir}
              reldir=..
            else
              reldir=.
          fi
          ${reldir}/%{configure}

      build-commands:
        - |
          if [ -n "%{builddir}" ]; then
            cd %{builddir}
          fi
          %{make}

      install-commands:
        - |
          if [ -n "%{builddir}" ]; then
            cd %{builddir}
          fi
          %{make-install}

        - |
          %{delete_libtool_files}
  meson:
    variables:
      meson-global: |
        --buildtype=plain

      ninja: |
        ninja -v -j ${NINJAJOBS} -C %{build-dir}

sources:
  git_tag:
    config:
      checkout-submodules: False
      track-tags: True
